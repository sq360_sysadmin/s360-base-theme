// Documentation on theming Storybook
// @see https://storybook.js.org/docs/configurations/theming/

import { create } from '@storybook/theming';

export default create({
  base: 'dark',
  brandTitle: 'S360 Base Theme',
  brandUrl: '',
  brandImage: '',
});
