# Vendors Folder
Typically this folder should remain empty, however, if a package cannot be
installed via `NPM` or `Yarn`, place that package's folder here.

**THIS SHOULD BE A LAST RESORT**
