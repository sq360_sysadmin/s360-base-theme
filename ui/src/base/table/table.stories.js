import tableTwig from './_table.twig';

/**
 * Storybook Definition.
 */
export default { title: 'Foundation/Table' };

export const table = () => {
  return tableTwig();
}
