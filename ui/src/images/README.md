# Images Folder
Any site related images belong here. If you choose to organize images into
folders, beware that `Webpack` will flatten this directory in the `dist` folder.
This **WILL** affect the path inside any `*.scss` file, if applicable.

## Icons Folder
All `*.svg` files in this folder will be compiled into a single SVG Spritemap.
