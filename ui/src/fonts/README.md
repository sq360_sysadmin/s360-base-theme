# Fonts Folder
If a font family is not available via an online font foundry, place them here.
Fonts should be organized with a top-level folder named for the font family.

**NOTE:** No files in this folder should not produce any CSS output.

## Font Files
Please only use `*.woff` and `*.woff2` extensions.
