# Core
This folder contains all variables, placeholders, functions, mixins, etc.

**NOTE:** There should be zero CSS output from any file in this folder.

## TWIG Templates
There will NEVER be twig files for anything in this directory.

## SCSS Files
All `*.scss` files must begin with an underscore.

## Storybook Files
There will NEVER be a storybook files for anything in this directory.

## Webpack Entry Point Files
There will NEVER be an entry point for this directory.